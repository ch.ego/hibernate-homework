package ru.codeinside.hibernate_homework.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import ru.codeinside.hibernate_homework.dao.StudentDao;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.util.StudentUtil;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class StudentServiceTest {

    @Autowired
    private StudentService studentService;
    @Autowired
    private UniversityService universityService;
    @Autowired
    private StudentDao studentDao;

    Student student = new Student(null, "test2", "test2", "test2", 20, null);
    Student student2 = new Student(null, "test2", "test2", "test2", 20, null);

    @Test
    @Transactional
    void findAll() {
        studentDao.update(student);
        assertEquals(1, studentService.findAll().size());
        studentDao.update(student2);
        assertEquals(2, studentService.findAll().size());
    }

    @Test
    @Transactional
    void findById() {
        Long id = studentService.create(new StudentRqDto("test", "test", "test", 22));
        assertDoesNotThrow(() -> studentService.getById(id));
        assertEquals("test test test", studentService.getById(id).getFullName());
    }

    @Test
    @Transactional
    void create() {
        studentService.create(new StudentRqDto("test", "test", "test", 20));
        assertEquals(1, studentDao.findAll().size());
        assertEquals("test", studentDao.findAll().get(0).getFirstName());
    }

    @Test
    @Transactional
    void update() {
        Long id = studentService.create(new StudentRqDto("test", "test", "test", 22));
        studentService.update(id, new StudentRqDto("updated", "updated", "updated", 22));
        assertEquals("updated updated updated", studentService.getById(id).getFullName());
    }

    @Test
    @Transactional
    void delete() {
        studentDao.update(student);
        student2 = studentDao.update(student2);
        studentService.delete(student2.getId());
        assertEquals(1, studentDao.findAll().size());
        assertEquals(student.getFirstName(), studentDao.findAll().get(0).getFirstName());
    }

    @Test
    @Transactional
    void addStudentToUniversity() {
        Long universityId = universityService.create(new UniversityRqDto("test", false));
        Long studentId = studentDao.update(student).getId();
        studentService.addStudentToUniversity(universityId, studentId);

        assertEquals(universityId, studentService.getById(studentId).getUniversityShortRsDto().getId());
        assertFalse(universityService.getById(universityId).getStudents().isEmpty());
    }

    @Test
    @Transactional
    void removeStudentFromUniversity() {
        Long universityId = universityService.create(new UniversityRqDto("test", false));
        Long student1Id = studentDao.update(student).getId();
        Long student2Id = studentDao.update(student2).getId();
        studentService.addStudentToUniversity(universityId, student1Id);
        studentService.addStudentToUniversity(universityId, student2Id);
        assertEquals(2, universityService.getById(universityId).getStudents().size());

        studentService.removeStudentFromUniversity(universityId, student2Id);
        assertEquals(studentService.getById(student1Id).getFullName(), universityService.getById(universityId).getStudents().get(0).getFullName());
    }
}
