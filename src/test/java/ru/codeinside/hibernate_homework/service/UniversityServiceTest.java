package ru.codeinside.hibernate_homework.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import ru.codeinside.hibernate_homework.dao.UniversityDao;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.exceptions.ConflictException;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UniversityServiceTest {

    @Autowired
    private StudentService studentService;
    @Autowired
    private UniversityService universityService;
    @Autowired
    private UniversityDao universityDao;

    University university = new University(null, "test", false, new ArrayList<>());
    University university2 = new University(null, "test", false, new ArrayList<>());

    @Test
    @Transactional
    void findAll() {
        universityDao.update(university);
        universityDao.update(university2);
        assertEquals(2, universityService.findAll().size());
    }

    @Test
    @Transactional
    void findById() {
        Long id = universityService.create(new UniversityRqDto("test", false));
        assertFalse(Optional.ofNullable(universityService.getById(id)).isEmpty());
    }

    @Test
    @Transactional
    void create() {
        Long id = universityService.create(new UniversityRqDto("test", false));
        assertEquals("test", universityService.getById(id).getName());
    }

    @Test
    @Transactional
    void update() {
        Long id = universityService.create(new UniversityRqDto("test", false));
        universityService.update(id, new UniversityRqDto("UPDATED", false));
        assertEquals("UPDATED", universityService.getById(id).getName());
    }

    @Test
    @Transactional
    void delete() {
        Long id = universityService.create(new UniversityRqDto("test", false));
        assertEquals("test", universityService.getById(id).getName());
        universityService.delete(id);
        assertThrows(NotFoundException.class, () -> universityService.getById(id));
    }

    @Test
    @Transactional
    void close() {
        Long studentId = studentService.create(new StudentRqDto("test", "test", "test", 22));
        Long universityId = universityService.create(new UniversityRqDto("test", false));
        studentService.addStudentToUniversity(universityId, studentId);
        assertEquals(1, universityService.getById(universityId).getStudents().size());
        universityService.close(universityId);
        assertNull(studentService.getById(studentId).getUniversityShortRsDto());
        assertThrows(ConflictException.class, () -> studentService.addStudentToUniversity(universityId, studentId));
    }
}