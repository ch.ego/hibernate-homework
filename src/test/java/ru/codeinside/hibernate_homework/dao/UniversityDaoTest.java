package ru.codeinside.hibernate_homework.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Import(UniversityDaoImpl.class)
class UniversityDaoTest {

    @Autowired
    TestEntityManager entityManager;
    @Autowired
    UniversityDao universityDao;

    University university = new University(null, "test", false, null);
    University university2 = new University(null, "test2", false, null);

    @Test
    void findById() {
        Long id = entityManager.persistAndGetId(university, Long.class);
        assertEquals(university.getName(), universityDao.findById(id).getName());
        assertThrows(NotFoundException.class, () -> universityDao.findById(87L));
    }

    @Test
    void findAll() {
        entityManager.persist(university);
        entityManager.persist(university2);
        assertEquals(2, universityDao.findAll().size());
    }

    @Test
    void update() {
        Long id = entityManager.persistAndGetId(university, Long.class);
        university.setName("UPDATED");
        universityDao.update(university);
        assertEquals(1, universityDao.findAll().size());
        assertEquals(university.getName(), universityDao.findById(id).getName());
    }

    @Test
    void delete() {
        entityManager.persist(university);
        Long id = entityManager.persistAndGetId(university2, Long.class);
        universityDao.delete(id);
        assertEquals(1, universityDao.findAll().size());
        assertEquals(university.getName(), universityDao.findAll().get(0).getName());
    }
}
