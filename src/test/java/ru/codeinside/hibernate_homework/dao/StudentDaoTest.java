package ru.codeinside.hibernate_homework.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;
import ru.codeinside.hibernate_homework.util.StudentUtil;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Import(StudentDaoImpl.class)
class StudentDaoTest {

    @Autowired
    TestEntityManager entityManager;
    @Autowired
    StudentDao studentDao;

    Student student = new Student(null, "test", "test", "test",20, null);
    Student student2 = new Student(null, "test2", "test2", "test2",20, null);

    @Test
    void findById() {
        Long id = entityManager.persistAndGetId(student, Long.class);
        Student found = studentDao.findById(id);
        assertEquals(StudentUtil.getFullName(student), StudentUtil.getFullName(found));
        assertThrows(NotFoundException.class, () -> studentDao.findById(87L));
    }

    @Test
    void findAll() {
        entityManager.persist(student);
        entityManager.persist(student2);
        List<Student> found = studentDao.findAll();
        assertEquals(2, found.size());
        assertTrue(found.contains(student) && found.contains(student2));
    }

    @Test
    void update() {
        Long id = entityManager.persistAndGetId(student, Long.class);
        student.setFirstName("UPDATED");
        studentDao.update(student);
        assertEquals("UPDATED", studentDao.findById(id).getFirstName());
    }

    @Test
    void delete() {
        entityManager.persist(student);
        Long id = entityManager.persistAndGetId(student2, Long.class);
        studentDao.delete(id);
        assertEquals(1, studentDao.findAll().size());
        assertEquals(StudentUtil.getFullName(student), StudentUtil.getFullName(studentDao.findAll().get(0)));
    }
}
