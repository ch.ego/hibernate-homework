package ru.codeinside.hibernate_homework.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.service.StudentService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = StudentController.class)
class StudentControllerTest {

    public static final String MAIN_API_PATH = "/api/v1/students";
    @MockBean
    private StudentService studentService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get(MAIN_API_PATH))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        StudentRqDto studentRqDto = new StudentRqDto("test", "test", "test", 22);
        Long id = studentService.create(studentRqDto);
        this.mockMvc.perform(get(MAIN_API_PATH + "/" + id))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        StudentRqDto studentRqDto = new StudentRqDto("test", "test", "test", 22);
        mockMvc.perform(
                        post(MAIN_API_PATH)
                                .content(objectMapper.writeValueAsString(studentRqDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isCreated());
    }

    @Test
    void update() throws Exception {
        StudentRqDto studentRqDto = new StudentRqDto("test", "test", "test", 22);
        Long id = studentService.create(studentRqDto);
        mockMvc.perform(
                        patch(MAIN_API_PATH + "/" + id)
                                .content(objectMapper.writeValueAsString(studentRqDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
    }

    @Test
    void deleteById() throws Exception {
        StudentRqDto studentRqDto = new StudentRqDto("test", "test", "test", 22);
        Long id = studentService.create(studentRqDto);
        mockMvc.perform(
                        delete(MAIN_API_PATH + "/" + id)
                                .content(objectMapper.writeValueAsString(studentRqDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isAccepted());
    }
}
