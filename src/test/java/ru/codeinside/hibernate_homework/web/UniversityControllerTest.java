package ru.codeinside.hibernate_homework.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.service.StudentService;
import ru.codeinside.hibernate_homework.service.UniversityService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UniversityController.class)
class UniversityControllerTest {

    public static final String MAIN_API_PATH = "/api/v1/universities";

    @MockBean
    private UniversityService universityService;
    @MockBean
    private StudentService studentService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get(MAIN_API_PATH))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        UniversityRqDto universityRqDto = new UniversityRqDto("test", false);
        Long id = universityService.create(universityRqDto);
        this.mockMvc.perform(get(MAIN_API_PATH + "/" + id))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        UniversityRqDto universityRqDto = new UniversityRqDto("test", false);
        mockMvc.perform(
                        post(MAIN_API_PATH)
                                .content(objectMapper.writeValueAsString(universityRqDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isCreated());
    }

    @Test
    void update() throws Exception {
        UniversityRqDto universityRqDto = new UniversityRqDto("test", false);
        Long id = universityService.create(universityRqDto);
        mockMvc.perform(
                        patch(MAIN_API_PATH + "/" + id)
                                .content(objectMapper.writeValueAsString(universityRqDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
    }

    @Test
    void deleteById() throws Exception {
        UniversityRqDto universityRqDto = new UniversityRqDto("test", false);
        Long id = universityService.create(universityRqDto);
        mockMvc.perform(
                        delete(MAIN_API_PATH + "/" + id)
                                .content(objectMapper.writeValueAsString(universityRqDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isAccepted());
    }

    @Test
    void addStudentToUniversity() throws Exception {
        Long universityId = universityService.create(new UniversityRqDto("test", false));
        Long studentId = studentService.create(new StudentRqDto("test", "test", "test", 22));
        mockMvc.perform(
                        post(MAIN_API_PATH + "/" + universityId + "/" + studentId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
    }

    @Test
    void removeStudentToUniversity() throws Exception {
        Long universityId = universityService.create(new UniversityRqDto("test", false));
        Long studentId = studentService.create(new StudentRqDto("test", "test", "test", 22));
        mockMvc.perform(
                        delete(MAIN_API_PATH + "/" + universityId + "/" + studentId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isAccepted());
    }

    @Test
    void closeUniversity() throws Exception {
        Long universityId = universityService.create(new UniversityRqDto("test", false));
        mockMvc.perform(
                        post(MAIN_API_PATH + "/" + universityId + "/close")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isAccepted());
    }
}
