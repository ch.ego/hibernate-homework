package ru.codeinside.hibernate_homework;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.codeinside.hibernate_homework.dao.StudentDao;
import ru.codeinside.hibernate_homework.dao.UniversityDao;
import ru.codeinside.hibernate_homework.service.StudentService;
import ru.codeinside.hibernate_homework.service.UniversityService;
import ru.codeinside.hibernate_homework.web.StudentController;
import ru.codeinside.hibernate_homework.web.UniversityController;
;import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class HibernateHomeworkApplicationTests {
    private StudentController studentController;
    private UniversityController  universityController;
    private StudentService studentService;
    private UniversityService universityService;
    private StudentDao studentDao;
    private UniversityDao universityDao;

    @Autowired
    public HibernateHomeworkApplicationTests(StudentController studentController,
                                             UniversityController universityController,
                                             StudentService studentService,
                                             UniversityService universityService,
                                             StudentDao studentDao,
                                             UniversityDao universityDao) {
        this.studentController = studentController;
        this.universityController = universityController;
        this.studentService = studentService;
        this.universityService = universityService;
        this.studentDao = studentDao;
        this.universityDao = universityDao;
    }

    @Test
    void contextLoads() {
        assertNotNull(studentController);
        assertNotNull(universityController);
        assertNotNull(studentService);
        assertNotNull(universityService);
        assertNotNull(studentDao);
        assertNotNull(universityDao);
    }
}
