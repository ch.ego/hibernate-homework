package ru.codeinside.hibernate_homework.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.codeinside.hibernate_homework.dao.StudentDao;
import ru.codeinside.hibernate_homework.dao.UniversityDao;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.dto.StudentFullRsDto;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.StudentShortRsDto;
import ru.codeinside.hibernate_homework.exceptions.ConflictException;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;
import ru.codeinside.hibernate_homework.mapper.StudentMapper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentDao studentDao;
    private final UniversityDao universityDao;
    private final StudentMapper studentMapper;

    @Autowired
    public StudentServiceImpl(StudentDao studentDao,
                              UniversityDao universityDao,
                              StudentMapper studentMapper) {
        this.studentDao = studentDao;
        this.universityDao = universityDao;
        this.studentMapper = studentMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudentShortRsDto> findAll() {
        return studentDao.findAll().stream()
                .map(studentMapper::toShortRsDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public StudentFullRsDto getById(Long id) {
        return studentMapper.toFullRsDto(studentDao.findById(id));
    }

    @Override
    @Transactional
    public Long create(StudentRqDto studentRqDto) {
        Student student = studentDao.update(studentMapper.toStudent(studentRqDto));
        return student.getId();
    }

    @Override
    @Transactional
    public StudentFullRsDto update(Long id, StudentRqDto studentRqDto) {
        if (Optional.ofNullable(studentDao.findById(id)).isEmpty()) {
            throw new NotFoundException("Студент не найден");
        }
        Student student = studentMapper.toStudent(studentRqDto);
        student.setId(id);
        return studentMapper.toFullRsDto(studentDao.update(student));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        studentDao.delete(id);
    }

    @Override
    @Transactional
    public void addStudentToUniversity(Long universityId, Long studentId) {
        Student student = studentDao.findById(studentId);
        University university = universityDao.findById(universityId);
        if (university.getClosed()) {
            throw new ConflictException("Университет закрыт");
        }
        student.setUniversity(university);
        studentDao.update(student);

        List<Student> students = university.getStudents();
        students.add(student);
        university.setStudents(students);
        universityDao.update(university);
    }

    @Override
    @Transactional
    public void removeStudentFromUniversity(Long universityId, Long studentId) {
        Student student = studentDao.findById(studentId);
        University university = universityDao.findById(universityId);
        if (!Objects.equals(student.getUniversity().getId(), university.getId())) {
            throw new ConflictException("В этом университете отсутствует такой студент");
        }
        student.setUniversity(null);
        studentDao.update(student);
        List<Student> students = university.getStudents();
        students.remove(student);
        universityDao.update(university);
    }
}
