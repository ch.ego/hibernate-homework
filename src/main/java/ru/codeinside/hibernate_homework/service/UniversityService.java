package ru.codeinside.hibernate_homework.service;

import ru.codeinside.hibernate_homework.dto.UniversityFullRsDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityShortRsDto;

import java.util.List;

public interface UniversityService {

    List<UniversityShortRsDto> findAll();

    UniversityFullRsDto getById(Long id);

    Long create(UniversityRqDto universityRqDto);

    UniversityFullRsDto update(Long id, UniversityRqDto universityRqDto);

    void delete(Long id);

    void close(Long id);
}
