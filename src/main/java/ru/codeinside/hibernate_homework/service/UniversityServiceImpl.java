package ru.codeinside.hibernate_homework.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.codeinside.hibernate_homework.dao.StudentDao;
import ru.codeinside.hibernate_homework.dao.UniversityDao;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.dto.UniversityFullRsDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityShortRsDto;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;
import ru.codeinside.hibernate_homework.mapper.StudentMapper;
import ru.codeinside.hibernate_homework.mapper.UniversityMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UniversityServiceImpl implements UniversityService {
    private final UniversityDao universityDao;
    private final UniversityMapper universityMapper;
    private final StudentDao studentDao;

    @Autowired
    public UniversityServiceImpl(UniversityDao universityDao, UniversityMapper universityMapper, StudentDao studentDao) {
        this.universityDao = universityDao;
        this.universityMapper = universityMapper;
        this.studentDao = studentDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<UniversityShortRsDto> findAll() {
        return universityDao.findAll()
                .stream()
                .map(universityMapper::toShortRsDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public UniversityFullRsDto getById(Long id) {
        return universityMapper.toFullRsDto(universityDao.findById(id));
    }

    @Override
    @Transactional
    public Long create(UniversityRqDto universityRqDto) {
        University university = universityDao.update(universityMapper.toUniversity(universityRqDto));
        return university.getId();
    }

    @Override
    @Transactional
    public UniversityFullRsDto update(Long id, UniversityRqDto universityRqDto) {
        if (Optional.ofNullable(universityDao.findById(id)).isEmpty()){
            throw new NotFoundException("Университет не найден");
        }
        University university = universityMapper.toUniversity(universityRqDto);
        university.setId(id);
        return universityMapper.toFullRsDto(universityDao.update(university));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        universityDao.delete(id);
    }

    @Override
    @Transactional
    public void close(Long id) {
        University university = universityDao.findById(id);
        university.setClosed(true);
        university.getStudents().forEach(student -> {
            student.setUniversity(null);
            studentDao.update(student);
        });
        university.setStudents(new ArrayList<>());
        universityDao.update(university);
    }
}
