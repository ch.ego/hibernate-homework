package ru.codeinside.hibernate_homework.service;

import ru.codeinside.hibernate_homework.dto.StudentFullRsDto;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.StudentShortRsDto;

import java.util.List;

public interface StudentService {

    List<StudentShortRsDto> findAll();

    StudentFullRsDto getById(Long id);

    Long create(StudentRqDto studentRqDto);

    StudentFullRsDto update(Long id, StudentRqDto studentRqDto);

    void delete(Long id);

    void addStudentToUniversity(Long universityId, Long studentId);

    void removeStudentFromUniversity(Long universityId, Long studentId);
}
