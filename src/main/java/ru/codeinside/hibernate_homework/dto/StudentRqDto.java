package ru.codeinside.hibernate_homework.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class StudentRqDto {

    @NotEmpty(message = "Поле не должно быть пустым")
    private String firstName;

    private String middleName;

    @NotEmpty(message = "Поле не должно быть пустым")
    private String lastName;

    @Min(value = 18, message = "Поле не должно быть меньше чем {value}")
    @Max(value = 50, message = "Поле не должно быть больше чем {value}")
    private Integer age;
}
