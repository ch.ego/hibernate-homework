package ru.codeinside.hibernate_homework.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Setter;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Builder
@Setter
public class StudentFullRsDto {
    private Long id;
    private String fullName;
    private Integer age;
    private UniversityShortRsDto universityShortRsDto;
}
