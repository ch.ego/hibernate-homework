package ru.codeinside.hibernate_homework.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class UniversityRqDto {
    @NotEmpty(message = "Поле не должно быть пустым")
    private String name;

    private Boolean closed;
}
