package ru.codeinside.hibernate_homework.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Builder
public class StudentShortRsDto {
    private Long id;
    private String fullName;
}
