package ru.codeinside.hibernate_homework.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Builder
public class UniversityShortRsDto {
    private Long id;
    private String name;
}
