package ru.codeinside.hibernate_homework.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@Builder
public class UniversityFullRsDto {
    private Long id;
    private String name;
    private Boolean closed;
    private List<StudentShortRsDto> students;
}
