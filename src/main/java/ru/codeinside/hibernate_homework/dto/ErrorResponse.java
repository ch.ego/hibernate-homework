package ru.codeinside.hibernate_homework.dto;

import lombok.Data;

@Data
public class ErrorResponse {
    private final String message;
}
