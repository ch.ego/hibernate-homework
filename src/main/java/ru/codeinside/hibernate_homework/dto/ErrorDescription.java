package ru.codeinside.hibernate_homework.dto;

import lombok.Data;

@Data
public class ErrorDescription {
    private final String field;
    private final String error;
}
