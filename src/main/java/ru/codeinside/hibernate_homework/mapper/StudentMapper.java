package ru.codeinside.hibernate_homework.mapper;

import org.springframework.stereotype.Component;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.dto.StudentFullRsDto;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.StudentShortRsDto;
import ru.codeinside.hibernate_homework.dto.UniversityShortRsDto;
import ru.codeinside.hibernate_homework.util.StudentUtil;

import java.util.Optional;

@Component
public class StudentMapper {

    public StudentShortRsDto toShortRsDto(Student student) {
        return StudentShortRsDto.builder()
                .id(student.getId())
                .fullName(StudentUtil.getFullName(student))
                .build();
    }

    public Student toStudent(StudentRqDto studentRqDto) {
        return Student.builder()
                .firstName(studentRqDto.getFirstName())
                .middleName(studentRqDto.getMiddleName())
                .lastName(studentRqDto.getLastName())
                .age(studentRqDto.getAge())
                .build();
    }

    public StudentFullRsDto toFullRsDto(Student student) {
        StudentFullRsDto student1 = StudentFullRsDto.builder()
                .id(student.getId())
                .fullName(StudentUtil.getFullName(student))
                .age(student.getAge())
                .build();
        if (Optional.ofNullable(student.getUniversity()).isPresent()) {
            student1.setUniversityShortRsDto(toShortRsUniDto(student.getUniversity()));
        }
        return student1;
    }

    public UniversityShortRsDto toShortRsUniDto(University university) {
        return UniversityShortRsDto.builder()
                .id(university.getId())
                .name(university.getName())
                .build();
    }
}
