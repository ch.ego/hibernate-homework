package ru.codeinside.hibernate_homework.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.dto.UniversityFullRsDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityShortRsDto;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UniversityMapper {

    private final StudentMapper studentMapper;

    @Autowired
    public UniversityMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    public UniversityShortRsDto toShortRsDto(University university) {
        return UniversityShortRsDto.builder()
                .id(university.getId())
                .name(university.getName())
                .build();
    }

    public UniversityFullRsDto toFullRsDto(University university) {
        UniversityFullRsDto universityFullRsDto = UniversityFullRsDto.builder()
                .id(university.getId())
                .name(university.getName())
                .closed(university.getClosed())
                .build();
        if (Optional.ofNullable(university.getStudents()).isPresent()) {
            universityFullRsDto.setStudents(university.getStudents()
                    .stream()
                    .map(studentMapper::toShortRsDto)
                    .collect(Collectors.toList()));
        }
        return universityFullRsDto;
    }

    public University toUniversity(UniversityRqDto rqDto) {
        return University.builder()
                .name(rqDto.getName())
                .closed(rqDto.getClosed())
                .students(new ArrayList<>())
                .build();
    }
}
