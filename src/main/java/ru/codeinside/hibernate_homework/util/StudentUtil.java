package ru.codeinside.hibernate_homework.util;

import ru.codeinside.hibernate_homework.domain.Student;

public final class StudentUtil {

    private StudentUtil() {
        throw new UnsupportedOperationException("Это служебный класс, экземпляр которого не может быть создан");
    }

    public static String getFullName(Student student) {
        return student.getFirstName() + " " + student.getMiddleName() + " " + student.getLastName();
    }
}
