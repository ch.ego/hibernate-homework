package ru.codeinside.hibernate_homework.dao;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.domain.University;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.util.List;
import java.util.Optional;

@Repository
public class UniversityDaoImpl implements UniversityDao {

    @Autowired
    EntityManager entityManager;

    @Override
    public University findById(Long id) {
        return Optional.ofNullable(entityManager.find(University.class, id))
                .orElseThrow(() -> new NotFoundException("Университет не найден"));
    }

    @Override
    public List<University> findAll() {
        return entityManager.createQuery("SELECT u from " + University.class.getSimpleName() + " u").getResultList();
    }

    @Override
    public University update(University university) {
        return entityManager.merge(university);
    }

    @Override
    public University delete(Long id) {
        University university = Optional.ofNullable(entityManager.find(University.class, id))
                .orElseThrow(() -> new NotFoundException("Университет не найден"));
        entityManager.remove(university);
        return university;
    }

    public University findUniversityByStudent(Student student){
        return Optional.ofNullable(entityManager.find(University.class, student))
                .orElseThrow(() -> new NotFoundException("Университет не найден"));
    }
}
