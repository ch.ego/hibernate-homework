package ru.codeinside.hibernate_homework.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.codeinside.hibernate_homework.domain.Student;
import ru.codeinside.hibernate_homework.exceptions.NotFoundException;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
public class StudentDaoImpl implements StudentDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Student findById(Long id) {
        return Optional.ofNullable(entityManager.find(Student.class, id))
                .orElseThrow(() -> new NotFoundException("Студент не найден"));
    }

    @Override
    public List<Student> findAll() {
        return entityManager.createQuery("SELECT s from " + Student.class.getSimpleName() + " s").getResultList();
    }

    @Override
    public Student update(Student student) {
        return entityManager.merge(student);
    }

    @Override
    public Student delete(Long id) {
        Student student = Optional.ofNullable(entityManager.find(Student.class, id))
                .orElseThrow(() -> new NotFoundException("Студент не найден"));
        entityManager.remove(student);
        return student;
    }
}
