package ru.codeinside.hibernate_homework.dao;

import org.springframework.data.repository.NoRepositoryBean;
import ru.codeinside.hibernate_homework.domain.Student;

@NoRepositoryBean
public interface StudentDao extends CrudOperations<Student, Long> {
}

