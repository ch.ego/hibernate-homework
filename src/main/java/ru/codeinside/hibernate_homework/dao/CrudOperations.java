package ru.codeinside.hibernate_homework.dao;

import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface CrudOperations<T, ID> {

    T findById(ID id);

    List<T> findAll();

    T update(T entity);

    T delete(ID id);
}
