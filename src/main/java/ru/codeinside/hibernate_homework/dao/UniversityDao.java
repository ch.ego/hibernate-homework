package ru.codeinside.hibernate_homework.dao;

import org.springframework.data.repository.NoRepositoryBean;
import ru.codeinside.hibernate_homework.domain.University;

@NoRepositoryBean
public interface UniversityDao extends CrudOperations<University, Long> {
}
