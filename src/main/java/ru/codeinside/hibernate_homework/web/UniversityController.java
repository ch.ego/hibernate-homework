package ru.codeinside.hibernate_homework.web;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.codeinside.hibernate_homework.dto.ErrorResponse;
import ru.codeinside.hibernate_homework.dto.UniversityFullRsDto;
import ru.codeinside.hibernate_homework.dto.UniversityRqDto;
import ru.codeinside.hibernate_homework.dto.UniversityShortRsDto;
import ru.codeinside.hibernate_homework.dto.ValidationErrorResponse;
import ru.codeinside.hibernate_homework.service.StudentService;
import ru.codeinside.hibernate_homework.service.UniversityService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Tag(name = "University REST API operations")
@RestController
@RequestMapping("/api/v1/universities")
@RequiredArgsConstructor
public class UniversityController {

    private final UniversityService universityService;
    private final StudentService studentService;

    @Operation(description = "Получение всех университетов", method = "GET")
    @ApiResponse(responseCode = "200", description = "OK",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = UniversityShortRsDto.class))))
    @GetMapping
    public List<UniversityShortRsDto> getAll() {
        return universityService.findAll();
    }

    @Operation(description = "Получение университета по id", method = "GET")
    @ApiResponse(responseCode = "200", description = "OK",
            content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = UniversityFullRsDto.class))})
    @ApiResponse(responseCode = "404", description = "NOT FOUND", content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    @GetMapping(value = "/{id}")
    public UniversityFullRsDto getById(@PathVariable Long id) {
        return universityService.getById(id);
    }

    @Operation(description = "Создание университета", method = "POST")
    @ApiResponse(responseCode = "201", description = "CREATED")
    @ApiResponse(responseCode = "400", description = "BAD REQUEST",
            content = @Content(schema = @Schema(implementation = ValidationErrorResponse.class)))
    @PostMapping
    public ResponseEntity<Void> create(@Valid @RequestBody UniversityRqDto request) {
        final Long id = universityService.create(request);
        final URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @Operation(description = "Изменение университета по id", method = "PATCH")
    @ApiResponse(responseCode = "200", description = "OK",
            content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = UniversityFullRsDto.class))})
    @ApiResponse(responseCode = "400", description = "BAD REQUEST",
            content = @Content(schema = @Schema(implementation = ValidationErrorResponse.class)))
    @ApiResponse(responseCode = "404", description = "NOT FOUND",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    @PatchMapping(value = "/{id}")
    public UniversityFullRsDto update(@PathVariable Long id, @Valid @RequestBody UniversityRqDto request) {
        return universityService.update(id, request);
    }

    @Operation(description = "Удаление университета по id", method = "DELETE")
    @ApiResponse(responseCode = "202", description = "ACCEPTED")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        universityService.delete(id);
        return ResponseEntity.accepted().build();
    }

    @Operation(description = "Добавление студента в университет", method = "POST")
    @ApiResponse(responseCode = "200", description = "OK")
    @PostMapping("/{universityId}/{studentId}")
    public void addStudentToUniversity(@PathVariable Long universityId, @PathVariable Long studentId) {
        studentService.addStudentToUniversity(universityId, studentId);
    }

    @Operation(description = "Удаление студента из университета", method = "POST")
    @ApiResponse(responseCode = "202", description = "ACCEPTED")
    @DeleteMapping("/{universityId}/{studentId}")
    public ResponseEntity<Object> removeStudentFromUniversity(@PathVariable Long universityId, @PathVariable Long studentId) {
        studentService.removeStudentFromUniversity(universityId, studentId);
        return ResponseEntity.accepted().build();
    }

    @Operation(description = "Закрытие университета и удаление всех студентов из него", method = "POST")
    @ApiResponse(responseCode = "202", description = "ACCEPTED")
    @PostMapping("/{id}/close")
    public ResponseEntity<Object> closeUniversity(@PathVariable Long id) {
        universityService.close(id);
        return ResponseEntity.accepted().build();
    }
}
