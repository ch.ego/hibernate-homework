package ru.codeinside.hibernate_homework.web;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.codeinside.hibernate_homework.dto.ErrorResponse;
import ru.codeinside.hibernate_homework.dto.StudentFullRsDto;
import ru.codeinside.hibernate_homework.dto.StudentRqDto;
import ru.codeinside.hibernate_homework.dto.StudentShortRsDto;
import ru.codeinside.hibernate_homework.dto.ValidationErrorResponse;
import ru.codeinside.hibernate_homework.service.StudentService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Tag(name = "Student REST API operations")
@RestController
@RequestMapping("/api/v1/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @Operation(description = "Получение всех студентов", method = "GET")
    @ApiResponse(responseCode = "200", description = "OK",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = StudentShortRsDto.class))))
    @GetMapping
    public List<StudentShortRsDto> getAll() {
        return studentService.findAll();
    }

    @Operation(description = "Получение студента по id", method = "GET")
    @ApiResponse(responseCode = "200", description = "OK",
            content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = StudentFullRsDto.class))})
    @ApiResponse(responseCode = "404", description = "NOT FOUND", content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    @GetMapping(value = "/{id}")
    public StudentFullRsDto getById(@PathVariable Long id) {
        return studentService.getById(id);
    }

    @Operation(description = "Создание студента", method = "POST")
    @ApiResponse(responseCode = "201", description = "CREATED")
    @ApiResponse(responseCode = "400", description = "BAD REQUEST",
            content = @Content(schema = @Schema(implementation = ValidationErrorResponse.class)))
    @PostMapping
    public ResponseEntity<Void> create(@Valid @RequestBody StudentRqDto studentRqDto) {
        final Long id = studentService.create(studentRqDto);
        final URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @Operation(description = "Изменение студента по id", method = "PATCH")
    @ApiResponse(responseCode = "200", description = "OK",
            content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = StudentFullRsDto.class))})
    @ApiResponse(responseCode = "400", description = "BAD REQUEST",
            content = @Content(schema = @Schema(implementation = ValidationErrorResponse.class)))
    @ApiResponse(responseCode = "404", description = "NOT FOUND",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    @PatchMapping(value = "/{id}")
    public StudentFullRsDto update(@PathVariable Long id, @Valid @RequestBody StudentRqDto studentRqDto) {
        return studentService.update(id, studentRqDto);
    }

    @Operation(description = "Удаление студента по id", method = "DELETE")
    @ApiResponse(responseCode = "202", description = "ACCEPTED")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        studentService.delete(id);
        return ResponseEntity.accepted().build();
    }
}
