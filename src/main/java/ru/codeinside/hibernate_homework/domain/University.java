package ru.codeinside.hibernate_homework.domain;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "universities")
public class University {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "closed",nullable = false)
    @Value("${some.key:false}")
    private Boolean closed;

    @OneToMany(mappedBy = "university", cascade = CascadeType.ALL)
    private List<Student> students;
}
